package com.devcamp.s50.invoicrestapi;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class InvoiceItemControl {
    @GetMapping("/invoices")
    public ArrayList<InvoiceItem> getEmployees(){

        //    3   sinh viên 

        InvoiceItem hoadon1 = new InvoiceItem("1022","but" , 10, 1000 );
        InvoiceItem hoadon2 = new InvoiceItem("1023","thuoc" , 16, 2000 );
        InvoiceItem hoadon3 = new InvoiceItem("1024","cap" , 9, 5000);
        // in thông tin của ba nhân viên
        System.out.println(hoadon1);
        System.out.println(hoadon2);
        System.out.println(hoadon3);
        // thêm ba nhân viên vào 1 danh sách

        ArrayList<InvoiceItem> invoiceItems = new ArrayList<InvoiceItem>();
        invoiceItems.add(hoadon1);
        invoiceItems.add(hoadon2);
        invoiceItems.add(hoadon3);
        return invoiceItems;
    }
    
}
