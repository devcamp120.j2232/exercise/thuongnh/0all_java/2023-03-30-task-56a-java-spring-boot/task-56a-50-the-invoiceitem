package com.devcamp.s50.invoicrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoicRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoicRestApiApplication.class, args);
	}

}
